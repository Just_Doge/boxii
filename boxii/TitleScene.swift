//
//  TitleScene.swift
//  boxii
//
//  Created by Geddy on 22/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import Foundation
import SpriteKit

var btnPlay: UIButton!
var gameTitle: UILabel!

class TitleScene: SKScene {
    
    override func didMove(to view: SKView) {
        self.backgroundColor = offBlackColor
        setUpText()
        
    }
    
    func setUpText() {
        btnPlay = UIButton(frame: CGRect(x: 100, y: 100, width: self.frame.width, height: 300))
        btnPlay.center = CGPoint(x: self.view!.frame.size.width / 2, y: 450)
        btnPlay.titleLabel?.font = UIFont(name: "Futura", size: 150)
        btnPlay.setTitle("play", for: .normal)
        btnPlay.setTitleColor( UIColor.green, for: .normal)
        btnPlay.backgroundColor = offWhiteColor
        
        btnPlay.addTarget(self, action: #selector(playTheGame), for: .touchUpInside)
        self.view?.addSubview(btnPlay)
        
        gameTitle = UILabel(frame: CGRect(x: 0, y: 0, width: (view?.frame.width)!, height: 300))
        gameTitle.textColor = offWhiteColor
        gameTitle.font = UIFont(name: "Futura", size: 130)
        gameTitle.textAlignment = .left
        gameTitle.text = "boxii"
        gameTitle.backgroundColor = UIColor.green
        
        self.view?.addSubview(gameTitle)
    }
    
    func playTheGame() {
        btnPlay.removeFromSuperview()
        gameTitle.removeFromSuperview()
        
        if let scene = GameScene(fileNamed: "GameScene") {
            let skView = self.view!
            
            skView.ignoresSiblingOrder = true
            
            scene.scaleMode = .aspectFill
            
            skView.presentScene(scene, transition: .fade(withDuration: 1.5))
        }
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
}
