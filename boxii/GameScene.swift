//
//  GameScene.swift
//  boxii
//
//  Created by Geddy on 22/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import SpriteKit
import GameplayKit


var block = SKSpriteNode()
var floor = SKSpriteNode()

var blockSize = CGSize(width: 70, height: 70)
var lblMain = SKLabelNode()
var lblScore = SKLabelNode()
var lblOffset = SKLabelNode()

var blockColor = UIColor()

var isSpawning = true
var isCollecting = false
var canComplete = false
var logic = false

var baseColor: CGFloat = 0.5

var offWhiteColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
var offBlackColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)

var touchLocation = CGPoint()

var score = 0

var countDownTime: Int = 6
var topCountDownTime: Int = 5

var touchedNode = SKNode()
var blocks = SKNode()

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        self.backgroundColor = offBlackColor
        
        let physicsBodyFrame = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.physicsBody = physicsBodyFrame
        
        spawnFloor()
        spawnLblMain()
        spawnLblScore()
        countDownTimer()
        self.addChild(blocks)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches {
            touchLocation = t.location(in: self)
            
            for sprite in blocks.children {
                if touchLocation == t.location(in: blocks) {
                    touchedNode = atPoint(touchLocation)
                    if isCollecting == true && touchedNode == sprite {
                        collectBlocks()
                    }
                }
            }
            
            if isSpawning {
                spawnBlock()
            }
            
            
        }
    }
    
    func spawnFloor() {
        floor = SKSpriteNode(color: offWhiteColor, size: CGSize(width: self.frame.width, height: 200))
        floor.position = CGPoint(x: self.frame.midX, y: self.frame.minY + 30)
        floor.physicsBody = SKPhysicsBody(rectangleOf: floor.size)
        floor.physicsBody?.restitution = 0.3
        floor.physicsBody?.affectedByGravity = false
        floor.physicsBody?.isDynamic = true
        floor.physicsBody?.allowsRotation = false
        
        floor.name = "floorName"
        self.addChild(floor)
        
    }
    
    func spawnBlock() {
        
        let red = CGFloat(arc4random_uniform(255)) + baseColor
        let blue = CGFloat(arc4random_uniform(255)) + baseColor
        let green = CGFloat(arc4random_uniform(255)) + baseColor
        
        block = SKSpriteNode(color: UIColor(red: red / 255, green: green / 25, blue: blue / 255, alpha: 1.0), size: blockSize)
        block.position = touchLocation
        block.physicsBody = SKPhysicsBody(rectangleOf: block.size)
        block.physicsBody?.affectedByGravity = true
        block.physicsBody?.isDynamic = true
        block.physicsBody?.allowsRotation = true
        block.name = "block"
        blocks.addChild(block)
        
    }
    
    func spawnLblMain() {
        lblMain = SKLabelNode(fontNamed: "Futura")
        lblMain.fontSize = 50
        lblMain.fontColor = offWhiteColor
        lblMain.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 150)
        lblMain.text = "Drop! 6"
        self.addChild(lblMain)
    }
    
    func spawnLblScore() {
        lblScore = SKLabelNode(fontNamed: "Futura")
        lblScore.fontSize = 50
        lblScore.fontColor = offBlackColor
        lblScore.position = CGPoint(x: self.frame.midX, y: self.frame.minY + 30)
        lblScore.text = "Score: \(score)"
        
        self.addChild(lblScore)
    }
    
    func spawnLblOffset() {
        lblOffset = SKLabelNode(fontNamed: "Futura")
        lblOffset.fontSize = 54
        lblOffset.fontColor = offWhiteColor
        lblOffset.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 150)
        lblOffset.text = "Drop! 6"
        self.addChild(lblOffset)
    }
    
    func countDownTimer() {
        let wait = SKAction.wait(forDuration: 1.0)
        let countDown = SKAction.run {
            countDownTime -= 1
            
            if countDownTime < 0 {
                countDownTime = topCountDownTime
                if isSpawning == true && isCollecting == false {
                    isSpawning = false
                    isCollecting = true
                    lblMain.text = "Drop! \(countDownTime)"
                } else if isSpawning == false && isCollecting == true && logic == true {
                    isSpawning = false
                    isCollecting = false
                    canComplete = true
                    lblMain.text = "Collect! \(countDownTime)"
                    self.gameOverLogic()
                }
                
                logic = true
            }
            
            if canComplete == false && isSpawning == true {
                lblMain.text = "Drop! \(countDownTime)"
            } else if canComplete == false && isSpawning == false {
                lblMain.text = "Collect! \(countDownTime)"
            }
        }
        
        let sequence = SKAction.sequence([wait, countDown])
        self.run(SKAction.repeatForever(sequence))
        
    }
    
    func gameOverLogic() {
        lblMain.fontSize = 120
        lblMain.text = "Done!"
        
        waitMoveToTitleScene()
    }
    
    func waitMoveToTitleScene() {
        let wait = SKAction.wait(forDuration: 2.5)
        let moveToTitleScene = SKAction.run {
            if let scene = TitleScene(fileNamed: "TitleScene") {
                let skView = self.view!
                
                skView.ignoresSiblingOrder = true
                
                scene.scaleMode = .aspectFill
                
                skView.presentScene(scene, transition: .fade(withDuration: 1.5))
            }

        }
        
        let sequence = SKAction.sequence([wait, moveToTitleScene])
        self.run(SKAction.repeat(sequence, count: 1))
    }
    
    func collectBlocks() {
        touchedNode.removeFromParent()
        
        score += 1
        updateScore()
    }
    
    
    func updateScore() {
        lblScore.text = "Score: \(score)"
    }
    
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
